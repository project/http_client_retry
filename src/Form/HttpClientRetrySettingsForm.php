<?php

namespace Drupal\http_client_retry\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\RfcLogLevel;
use GuzzleRetry\GuzzleRetryMiddleware;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides a general admin form for the HTTP Client Retry module.
 *
 * @package Drupal\http_client_retry\Form
 */
class HttpClientRetrySettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'http_client_retry_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['http_client_retry.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->configFactory->get('http_client_retry.settings');

    $form['retry_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable retries'),
      '#description' => $this->t('Activate retries for all HTTP client requests using configured settings.'),
      '#default_value' => $config->get('request_defaults.retry_enabled'),
      '#disabled' => $this->configIsOverridden('request_defaults.retry_enabled'),
    ];

    $form['defaults'] = [
      '#type' => 'details',
      '#title' => $this->t('Retry defaults'),
      '#open' => TRUE,
    ];

    $form['defaults']['max_retry_attempts'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum retries'),
      '#description' => $this->t('Maximum number of retries to attempt for a request.'),
      '#step' => 1,
      '#min' => 1,
      '#size' => 4,
      '#default_value' => $config->get('request_defaults.max_retry_attempts'),
      '#disabled' => $this->configIsOverridden('request_defaults.max_retry_attempts'),
      '#states' => [
        'required' => [
          ':input[name="retry_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['defaults']['default_retry_multiplier'] = [
      '#type' => 'number',
      '#title' => $this->t('Retry after multiplier'),
      '#description' => $this->t('Multiplier for wait time between retries.'),
      '#step' => 'any',
      '#min' => 1,
      '#size' => 6,
      '#default_value' => $config->get('request_defaults.default_retry_multiplier'),
      '#disabled' => $this->configIsOverridden('request_defaults.default_retry_multiplier'),
    ];

    $form['defaults']['max_allowable_timeout_secs'] = [
      '#type' => 'number',
      '#title' => $this->t('Maximum timeout'),
      '#description' => $this->t('Maximum total number of seconds to attempt retries for a request. Leave blank for no timeout.'),
      '#step' => 1,
      '#min' => 1,
      '#size' => 4,
      '#default_value' => $config->get('request_defaults.max_allowable_timeout_secs'),
      '#disabled' => $this->configIsOverridden('request_defaults.max_allowable_timeout_secs'),
    ];

    $form['defaults']['expose_retry_header'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add retry count header'),
      '#description' => $this->t('Add the retry count to a header on the response.'),
      '#default_value' => $config->get('request_defaults.expose_retry_header'),
      '#disabled' => $this->configIsOverridden('request_defaults.expose_retry_header'),
    ];

    $form['defaults']['retry_header'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Retry count header'),
      '#description' => $this->t('Header name to use for retry counter.'),
      '#default_value' => $config->get('request_defaults.retry_header'),
      '#disabled' => $this->configIsOverridden('request_defaults.retry_header'),
      '#attributes' => [
        'placeholder' => GuzzleRetryMiddleware::RETRY_HEADER,
      ],
      '#states' => [
        'visible' => [
          ':input[name="expose_retry_header"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['defaults']['retry_only_if_retry_after_header'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Respect "Retry-After" header'),
      '#description' => $this->t('Only retry if the "Retry after" is present on the response.'),
      '#default_value' => $config->get('request_defaults.retry_only_if_retry_after_header'),
      '#disabled' => $this->configIsOverridden('request_defaults.retry_only_if_retry_after_header'),
    ];

    $form['defaults']['retry_after_header'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Retry after header'),
      '#description' => $this->t('Header name to expect on responses for retry after information.'),
      '#default_value' => $config->get('request_defaults.retry_after_header'),
      '#disabled' => $this->configIsOverridden('request_defaults.retry_after_header'),
      '#attributes' => [
        'placeholder' => GuzzleRetryMiddleware::RETRY_AFTER_HEADER,
      ],
      '#states' => [
        'visible' => [
          ':input[name="retry_only_if_retry_after_header"]' => ['checked' => TRUE],
        ],
      ],
    ];

    $form['conditions'] = [
      '#type' => 'details',
      '#title' => $this->t('Retry conditions'),
      '#open' => FALSE,
    ];

    $form['conditions']['retry_on_timeout'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Retry on timeout'),
      '#description' => $this->t('Retry when a request times out.'),
      '#default_value' => $config->get('request_defaults.retry_on_timeout'),
      '#disabled' => $this->configIsOverridden('request_defaults.retry_on_timeout'),
    ];

    $response_options = array_filter(Response::$statusTexts, function ($code) {
      return $code % 400 < 100 || $code % 500 < 100;
    }, ARRAY_FILTER_USE_KEY);
    $form['conditions']['retry_on_status'] = [
      '#type' => 'select',
      '#title' => $this->t('Retry responses'),
      '#description' => $this->t('Response statuses that trigger a retry.'),
      '#options' => $response_options,
      '#multiple' => TRUE,
      '#size' => count($response_options) / 2,
      '#default_value' => $config->get('request_defaults.retry_on_status'),
      '#disabled' => $this->configIsOverridden('request_defaults.retry_on_status'),
    ];

    $form['logging'] = [
      '#type' => 'details',
      '#title' => $this->t('Logging'),
      '#open' => TRUE,
    ];

    $form['logging']['logging_enabled'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Log retries'),
      '#description' => $this->t('Log retries to the Drupal system log.'),
      '#default_value' => $config->get('logging.enabled'),
      '#disabled' => $this->configIsOverridden('logging.enabled'),
    ];

    $form['logging']['logging_level'] = [
      '#type' => 'select',
      '#title' => $this->t('Log level'),
      '#description' => $this->t('Log level to use when logging retries.'),
      '#options' => RfcLogLevel::getLevels(),
      '#default_value' => $config->get('logging.level'),
      '#disabled' => $this->configIsOverridden('logging.level'),
      '#states' => [
        'visible' => [
          ':input[name="logging_enabled"]' => ['checked' => TRUE],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->config('http_client_retry.settings');

    $bools = [
      'request_defaults.retry_enabled' => 'retry_enabled',
      'request_defaults.retry_on_timeout' => 'retry_on_timeout',
      'request_defaults.expose_retry_header' => 'expose_retry_header',
      'request_defaults.retry_only_if_retry_after_header' => 'retry_only_if_retry_after_header',
      'logging.enabled' => 'logging_enabled',
    ];
    foreach ($bools as $config_name => $field_name) {
      if (!$this->configIsOverridden($config_name)) {
        $config->set($config_name, (bool) $form_state->getValue($field_name));
      }
    }

    $ints = [
      'request_defaults.max_retry_attempts' => 'max_retry_attempts',
      'request_defaults.max_allowable_timeout_secs' => 'max_allowable_timeout_secs',
      'logging.level' => 'logging_level',
    ];
    foreach ($ints as $config_name => $field_name) {
      if (!$this->configIsOverridden($config_name)) {
        $value = $form_state->getValue($field_name);
        if ($value === '0' || !empty($value)) {
          $value = (int) $value;
        }
        else {
          $value = NULL;
        }
        $config->set($config_name, $value);
      }
    }

    $floats = [
      'request_defaults.default_retry_multiplier' => 'default_retry_multiplier',
    ];
    foreach ($floats as $config_name => $field_name) {
      if (!$this->configIsOverridden($config_name)) {
        $value = $form_state->getValue($field_name);
        if ($value === '0' || !empty($value)) {
          $value = (float) $value;
        }
        else {
          $value = NULL;
        }
        $config->set($config_name, $value);
      }
    }

    $no_casts = [
      'request_defaults.retry_on_status' => 'retry_on_status',
      'request_defaults.retry_after_header' => 'retry_after_header',
      'request_defaults.retry_header' => 'retry_header',
    ];
    foreach ($no_casts as $config_name => $field_name) {
      if (!$this->configIsOverridden($config_name)) {
        $value = $form_state->getValue($field_name);
        if (empty($value)) {
          $value = NULL;
        }
        $config->set($config_name, $value);
      }
    }

    $config->save();
    parent::submitForm($form, $form_state);
  }

  /**
   * Check if a config variable is overridden by local settings.
   *
   * @param string $name
   *   Settings key to check.
   *
   * @return bool
   *   TRUE if the config is overwritten, FALSE otherwise.
   */
  protected function configIsOverridden(string $name): bool {
    $original = $this->configFactory
      ->getEditable('http_client_retry.settings')
      ->get($name);
    $current = $this->configFactory
      ->get('http_client_retry.settings')
      ->get($name);
    return $original != $current;
  }

}
