<?php

namespace Drupal\http_client_retry\Event;

/**
 * Defines events for the HTTP Client Retry module.
 *
 * @package Drupal\http_client_retry\Event
 */
final class RequestEvents {

  /**
   * Name of event fired before a retry is attempted.
   *
   * @Event
   *
   * @var string
   */
  const RETRY = 'http_client_retry.request.retry';

}
