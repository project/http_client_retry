<?php

namespace Drupal\http_client_retry\Event;

use Drupal\Component\EventDispatcher\Event;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

/**
 * Event triggered before a request is retried.
 *
 * @package Drupal\http_client_retry\Event
 */
class RequestRetryEvent extends Event {

  /**
   * Current retry count (including the try about to take place!).
   *
   * @var int
   */
  protected int $retryCount;

  /**
   * Delay timeout.
   *
   * @var float
   */
  protected float $delayTimeout;

  /**
   * Request object.
   *
   * @var \Psr\Http\Message\RequestInterface
   */
  protected RequestInterface $request;

  /**
   * Configured client options.
   *
   * @var array
   */
  protected array $options;

  /**
   * Response object.
   *
   * @var \Psr\Http\Message\ResponseInterface|null
   */
  protected ?ResponseInterface $response = NULL;

  /**
   * RequestRetryEvent constructor.
   *
   * @see \GuzzleRetry\GuzzleRetryMiddleware::doRetry()
   */
  public function __construct(
    int $retry_count,
    float $delay_timeout,
    RequestInterface $request,
    array $options,
    ?ResponseInterface $response = NULL,
  ) {
    $this->retryCount = $retry_count;
    $this->delayTimeout = $delay_timeout;
    $this->request = $request;
    $this->options = $options;
    $this->response = $response;
  }

  /**
   * Gets the total retry count for the request (including this retry).
   *
   * @return int
   *   Total retry count for the request (including this retry).
   */
  public function getRetryCount(): int {
    return $this->retryCount;
  }

  /**
   * Gets the configured delay timeout for this request.
   *
   * @return float
   *   Delay timeout for this request.
   */
  public function getDelayTimeout(): float {
    return $this->delayTimeout;
  }

  /**
   * Gets the request object being retried.
   *
   * @return \Psr\Http\Message\RequestInterface
   *   Request object.
   */
  public function getRequest(): RequestInterface {
    return $this->request;
  }

  /**
   * Gets the configured options for the retries.
   *
   * @return array
   *   Retry options.
   */
  public function getOptions(): array {
    return $this->options;
  }

  /**
   * Gets the most recent response object if one is available.
   *
   * @return \Psr\Http\Message\ResponseInterface|null
   *   Response object.
   */
  public function getResponse(): ?ResponseInterface {
    return $this->response;
  }

}
