<?php

namespace Drupal\http_client_retry\EventSubscriber;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\http_client_retry\Event\RequestEvents;
use Drupal\http_client_retry\Event\RequestRetryEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Logs retries if configured to do so.
 *
 * @package Drupal\http_client_retry\EventSubscriber
 */
class RequestRetryEventSubscriber implements EventSubscriberInterface {

  /**
   * HTTP Client Retry module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * HTTP Client Retry module logger.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected LoggerChannelInterface $logger;

  /**
   * HttpClientMiddleware constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory.
   * @param \Drupal\Core\Logger\LoggerChannelInterface $logger
   *   HTTP Client Retry module logger.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    LoggerChannelInterface $logger,
  ) {
    $this->config = $config_factory->get('http_client_retry.settings');
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [RequestEvents::RETRY => 'onRetry'];
  }

  /**
   * Logs information about retries if configured to do so.
   *
   * @param \Drupal\http_client_retry\Event\RequestRetryEvent $event
   *   Request retry event.
   */
  public function onRetry(RequestRetryEvent $event): void {
    $config = $this->config->get('logging') ?? [];
    if (($config['enabled'] ?? FALSE) != TRUE) {
      return;
    }

    $this->logger->log(
      $config['level'],
      'Retrying request to @request_uri. Server responded with status code @status_code. Will wait @delay_timeout seconds. This is attempt #@retry_count.', [
        '@request_uri' => $event->getRequest()->getUri(),
        '@status_code' => $event->getResponse()?->getStatusCode() ?? '(no response)',
        '@delay_timeout' => number_format($event->getDelayTimeout(), 2),
        '@retry_count' => $event->getRetryCount(),
      ]
    );
  }

}
