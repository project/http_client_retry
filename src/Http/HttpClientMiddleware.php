<?php

namespace Drupal\http_client_retry\Http;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\http_client_retry\Event\RequestEvents;
use Drupal\http_client_retry\Event\RequestRetryEvent;
use GuzzleRetry\GuzzleRetryMiddleware;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Instrument Guzzle HTTP requests.
 */
class HttpClientMiddleware {

  /**
   * HTTP Client Retry module config.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * HttpClientMiddleware constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event dispatcher.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EventDispatcherInterface $event_dispatcher,
  ) {
    $this->config = $config_factory->get('http_client_retry.settings');
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * Returns Guzzle Retry Middleware.
   *
   * @return \Closure
   *   Guzzle Retry Middleware factory.
   *
   * @see https://github.com/caseyamcl/guzzle_retry_middleware
   */
  public function __invoke(): \Closure {
    return GuzzleRetryMiddleware::factory($this->getConfiguredDefaults());
  }

  /**
   * Get configured defaults with suggested defaults as fallbacks.
   *
   * These options mostly follow the library defaults but diverge on a few for
   * convenience:
   *  - max_retry_attempts: 3 (default: 10) limits the total retry time to nine
   *    seconds instead of one minute, 22 seconds.
   *  - expose_retry_header: TRUE (default: FALSE) adds the `X-Retry-Counter`
   *    header to the response object.
   *
   * @return array
   *   HTTP Client request default configuration options.
   *
   * @see https://github.com/caseyamcl/guzzle_retry_middleware#options
   */
  private function getConfiguredDefaults(): array {
    $defaults = array_filter($this->config->get('request_defaults') ?? [], static fn($var) => $var !== NULL);
    return $defaults + [
      'retry_enabled' => TRUE,
      'max_retry_attempts' => 3,
      'max_allowable_timeout_secs' => NULL,
      'retry_only_if_retry_after_header' => FALSE,
      'retry_on_status' => [
        Response::HTTP_TOO_MANY_REQUESTS,
        Response::HTTP_SERVICE_UNAVAILABLE,
      ],
      'default_retry_multiplier' => 1.5,
      'on_retry_callback' => [$this, 'onRetry'],
      'retry_on_timeout' => FALSE,
      'expose_retry_header' => TRUE,
      'retry_header' => GuzzleRetryMiddleware::RETRY_HEADER,
      'retry_after_header' => GuzzleRetryMiddleware::RETRY_AFTER_HEADER,
      'retry_after_date_format' => GuzzleRetryMiddleware::DATE_FORMAT,
    ];
  }

  /**
   * Dispatch an event before each retry.
   *
   * @param int $retry_count
   *   Current retry count (including the try about to take place!).
   * @param float $delay_timeout
   *   Delay timeout.
   * @param \Psr\Http\Message\RequestInterface $request
   *   Request object.
   * @param array $options
   *   Configured client options.
   * @param \Psr\Http\Message\ResponseInterface|null $response
   *   Response object.
   *
   * @see \Drupal\http_client_retry\Event\RequestRetryEvent
   */
  public function onRetry(
    int $retry_count,
    float $delay_timeout,
    RequestInterface $request,
    array $options,
    ?ResponseInterface $response = NULL,
  ): void {
    $event = new RequestRetryEvent($retry_count, $delay_timeout, $request, $options, $response);
    $this->eventDispatcher->dispatch($event, RequestEvents::RETRY);
  }

}
