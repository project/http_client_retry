CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Configuration
* Functionality
* Maintainers

INTRODUCTION
-----

HTTP Client Retry adds a middleware to the core
[`http.client` service](https://api.drupal.org/api/drupal/core%21core.services.yml/service/http_client/9.3.x)
to retry HTTP requests automatically when they fail with certain (configurable)
response status code.

REQUIREMENTS
------------

The
[Guzzle Retry Middleware](https://packagist.org/packages/caseyamcl/guzzle_retry_middleware)
package is required to be installed to use this module. It should be installed
automatically when adding this module via composer. If not, use the following
command to install it to your base repository:

    composer require caseyamcl/guzzle_retry_middleware

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

Visit Configuration » System » HTTP Client Retry
(`/admin/config/system/http_client_retry`) to enable and configure module
settings.

FUNCTIONALITY
-------------------

## Request retry event

Whenever a request is retried a `\Drupal\http_client_retry\Event\RequestRetryEvent`
event (`http_client_retry.request.retry`) is dispatched. This event includes
details about the retrying status, configuration information for the retries,
the original request object, and the current response object. Event subscribers
can be used to take action on this event (this is also how the module itself
handles optional retry logging).

## Per request configuration

The middleware provided by this module can also be configured on individual
requests using `http.client` service (i.e. in custom code). See the
[Guzzle Retry Middleware README file](https://github.com/caseyamcl/guzzle_retry_middleware#options)
for more details. For example, to only enable the retry functionality for
specific request the module configuration can set retries to disabled and then
pass configuration on the request like so:

```
$client = Drupal::service('http_client');
$client->request('GET', 'https://httpstat.us/503', ['retry_enabled' => TRUE]);
[warning] Retrying request to https://httpstat.us/503. Server responded with
          503. Will wait 1.50 seconds. This is attempt #1.
[warning] Retrying request to https://httpstat.us/503. Server responded with
          503. Will wait 3.00 seconds. This is attempt #2.
[warning] Retrying request to https://httpstat.us/503. Server responded with
          503. Will wait 4.50 seconds. This is attempt #3.
GuzzleHttp\Exception\ServerException with message 'Server error:
  `GET https://httpstat.us/503` resulted in a `503 Service Unavailable`
  response'
```

MAINTAINERS
-----------

Current maintainers:
* Christopher C. Wells (wells) - https://www.drupal.org/u/wells

This project has been sponsored by:
* Cascade Public Media
