<?php

namespace Drupal\http_client_retry_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Response;

/**
 * Provides status routes for the http_client_retry_test module.
 */
class StatusTestController extends ControllerBase {

  /**
   * Displays an Error 503 (Service unavailable) page.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response.
   */
  public function serviceUnavailable(): Response {
    $response = new Response();
    $response->setStatusCode(Response::HTTP_SERVICE_UNAVAILABLE);
    return $response;
  }

}
