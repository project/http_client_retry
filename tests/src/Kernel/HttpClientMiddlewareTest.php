<?php

namespace Drupal\Tests\http_client_retry\Kernel;

use Drupal\KernelTests\KernelTestBase;

/**
 * Tests for the HTTP Client middleware provided by HTTP Client Retry.
 *
 * @group http_client_retry
 */
class HttpClientMiddlewareTest extends KernelTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['http_client_retry'];

  /**
   * Tests if the middleware is added to the HTTP Client.
   */
  public function testMiddlewareIsAddedToHttpClient(): void {
    $client = \Drupal::httpClient();
    $reflector = new \ReflectionProperty(get_class($client), 'config');
    $reflector->setAccessible(TRUE);
    /** @var \GuzzleHttp\HandlerStack $handler */
    $handler = $reflector->getValue($client)['handler'];
    $this->assertStringContainsString(
      'http_client_middleware.http_client_retry',
      (string) $handler
    );
  }

}
