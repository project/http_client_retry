<?php

namespace Drupal\Tests\http_client_retry\Functional;

use Drupal\Core\Url;
use Drupal\Tests\BrowserTestBase;
use Drupal\user\UserInterface;

/**
 * Tests the module settings form.
 *
 * @group http_client_retry
 */
class HttpClientRetrySettingsFormTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['http_client_retry'];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * User with the permissions to edit module settings.
   *
   * @var \Drupal\user\UserInterface
   */
  protected UserInterface $adminUser;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser(['administer http client retry']);
  }

  /**
   * Tests if the module functionality can be disabled from the settings form.
   */
  public function testCanDisableRetries(): void {
    $this->drupalLogin($this->adminUser);

    $this->drupalGet(Url::fromRoute('http_client_retry.settings'));
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('xpath', '//input[@name="retry_enabled" and @checked]');

    $edit = ['retry_enabled' => 0];
    $this->submitForm($edit, 'Save configuration');

    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->pageTextContains('The configuration options have been saved.');
    $this->assertSession()->elementExists('xpath', '//input[@name="retry_enabled" and not(@checked)]');
  }

}
