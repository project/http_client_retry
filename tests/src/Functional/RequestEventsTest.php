<?php

namespace Drupal\Tests\http_client_retry\Functional;

use Drupal\Core\State\State;
use Drupal\Core\Url;
use Drupal\http_client_retry\Event\RequestEvents;
use Drupal\http_client_retry\Event\RequestRetryEvent;
use Drupal\Tests\BrowserTestBase;
use GuzzleHttp\Exception\ServerException;

/**
 * Tests for request events provided by HTTP Client Retry.
 *
 * @group http_client_retry
 */
class RequestEventsTest extends BrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * State service for recording information received by event listeners.
   *
   * @var \Drupal\Core\State\State
   */
  protected State $state;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['http_client_retry', 'http_client_retry_test'];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'standard';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->state = \Drupal::state();
    \Drupal::service('event_dispatcher')
      ->addListener(RequestEvents::RETRY, [$this, 'retryEventRecorder']);
  }

  /**
   * Tests if request retry event fires as expected.
   */
  public function testRequestRetryEventFires(): void {
    $client = \Drupal::httpClient();
    $url = Url::fromRoute('http_client_retry_test.service_unavailable', [], ['absolute' => TRUE]);

    try {
      $client->get($url->toString());
    }
    catch (ServerException $e) {
      // Ultimately the retries will still fail so we must catch this.
    }

    $last_event = $this->state->get('http_client_retry.last_event');
    $this->assertIsArray($last_event);
    $this->assertArrayHasKey('event_name', $last_event);
    $this->assertSame(RequestEvents::RETRY, $last_event['event_name']);
  }

  /**
   * Reacts to request retry event.
   *
   * @param \Drupal\http_client_retry\Event\RequestRetryEvent $event
   *   Event object.
   * @param string $name
   *   Event name.
   */
  public function retryEventRecorder(RequestRetryEvent $event, string $name) {
    $this->state->set('http_client_retry.last_event', ['event_name' => $name]);
  }

}
